const assert = require('assert');
const app = require('../../src/app');

describe('\'uncrawledDistances\' service', () => {
  it('registered the service', () => {
    const service = app.service('uncrawled-distances');

    assert.ok(service, 'Registered the service');
  });
});
