// Initializes the `uncrawledDistances` service on path `/uncrawled-distances`
const createService = require('feathers-mongoose');
const createModel = require('../../models/uncrawled-distances.model');
const hooks = require('./uncrawled-distances.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/uncrawled-distances', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('uncrawled-distances');

  service.hooks(hooks);
};
