

const getUncrawledDistances = require('../../hooks/get-uncrawled-distances');

module.exports = {
  before: {
    all: [],
    find: [getUncrawledDistances()],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
