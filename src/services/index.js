const distances = require('./distances/distances.service.js');
const crawlerDevices = require('./crawler-devices/crawler-devices.service.js');
const uncrawledDistances = require('./uncrawled-distances/uncrawled-distances.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(distances);
  app.configure(crawlerDevices);
  app.configure(uncrawledDistances);
};
