// Initializes the `crawler-devices` service on path `/crawler-devices`
const createService = require('feathers-mongoose');
const createModel = require('../../models/crawler-devices.model');
const hooks = require('./crawler-devices.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/crawler-devices', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('crawler-devices');
  service.hooks(hooks);

  setInterval(async function() {
    let allDevices = await service.find();
    console.log('>>>>> Found',allDevices.data.length,'devices in crawler devices collection');
    for (let i = 0; i < allDevices.data.length; i++) {
      if (allDevices.data[i].DistanceList.length === 0) {
        await service.remove({
          "_id": allDevices.data[i]._id
        })
        console.log('>>>>> Remove',allDevices.data[i].DeviceName,'successfully!');
      } 
    }
  }, 600*1000); 
};
