// crawler-devices-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const crawlerDevices = new Schema({
   DeviceName: String,
   DistanceList: [String],
   StartTime: Date
  }, {collection: 'crawler-devices'});

  return mongooseClient.model('crawlerDevices', crawlerDevices);
};
