// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    const { result } = context;
    const { Model } = context.app.service('crawler-devices')
    const myresult = await Model.updateMany({"DeviceName": context.data.DeviceName},
    {$pull: {"DistanceList": context.data.DistID}});
    context.result = myresult;
    return context;
  };
};
